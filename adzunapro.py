import requests


class AdzunaPro:

    def __init__(self, my_id, my_key):
        self.my_id = my_id
        self.my_key = my_key
        self.c_type = "application/json; charset=utf8"
        self.root_url = "http://api.adzuna.com:80/v1/api/jobs/"

    def query_string(dic):
        # moze to jos detaljnije TO DO:
        q = ''
        for k, v in dic.items():
            if v is None:
                pass
            else:
                q += str(k) + '=' + str(v) + '&'

        q = q.replace(' ', '%20')
        # print(q[:-1])
        return q[:-1]

    def search_ads(self, country=None, results_per_page=None,
                   what=None, what_exclude=None):
        params = {
            'app_id': self.my_id,
            'app_key': self.my_key,
            'results_per_page': results_per_page,
            'what': what,
            'what_exclude': what_exclude,
            }

        headers = {
            'Content-Type': self.c_type
            }

        url = self.root_url + country + '/search' + '/1?'
        # url2 = self.root_url + country + '/search' + '/1?' \
        #     + AdzunaPro.query_string(params)
        # params = dict([(k, v) for (k, v) in params.items()])
        # print(params)
        r = requests.get(url, headers=headers, params=params)
        # r2 = requests.get(url2, headers=headers)

        # print(r2.json())
        # print(r.json())
        # print(r.url)
        # print(r2.url)
        # print(r.text)
        return r.json()

    def histogram_data(self, country, location_0, location_1, what):
        params = {
            'app_id': self.my_id,
            'app_key': self.my_key,
            'location0': location_0,
            'location1': location_1,
            'what': what
            }

        headers = {
            'Content-Type': self.c_type
            }

        url = self.root_url + country + '/histogram?'

        r = requests.get(url, headers=headers, params=params)

        # print(r.text)
        # return r.json()
        return r.text
