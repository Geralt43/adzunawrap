from adzunapro import AdzunaPro
import matplotlib.pyplot as plt
import numpy as np
import json

APP_ID = 'XXX'
APP_KEY = 'XXX'

ap = AdzunaPro(APP_ID, APP_KEY)

# ap.search_ads(country, results_per_page, what)
# ap.histogram_data(country, Location_0, location_1, what)

a = ['Yorkshire And The Humber', 'Scotland', 'Wales',
     'Northern Ireland', 'West Midlands']


def plotter(locations):
    plot_lists = []
    for location in locations:
        histo_data = ap.histogram_data('gb', 'uk', location, 'javascript developer')
        data = json.loads(histo_data)
        to_plot = data['histogram']
        plot_lists.append(to_plot)
        # print(to_plot)
    # print(plot_lists)
    plt.xlabel('Salary')
    plt.ylabel('People')
    plt.title('JavaScript Developers')
    plt.style.use('seaborn-deep')
    i = 0
    j = 0
    c = ['red', 'blue', 'black', 'yellow', 'green']
    for dic in plot_lists:
        # numpy.ndarray != list
        plt.bar(np.arange(len(dic)) + i, dic.values(), color=c[j], width=0.15, label=a[j])
        plt.xticks(range(len(dic)), sorted(list(dic.keys())))
        i += 0.2
        j += 1
    plt.legend()
    plt.show()


plotter(a)
