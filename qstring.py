
def qs(a):
    q = ''
    for k, v in a.items():
        q += str(k) + '=' + str(v) + '&'

    q = q.replace(' ','%20')

    print(q[:-1])
    
    return q[:-1]

a =	{
  "brand": "Ford",
  "model": "Mustang 23",
  "year": 1964
}

qs(a)
